package test.test;

import java.net.MalformedURLException;
import java.net.URL;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;

import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;

/**
 * Unit test for simple App.
 */
public class AppTest 
    extends TestCase
{
    /**
     * Create the test case
     *
     * @param testName name of the test case
     */
    public AppTest( String testName )
    {
        super( testName );
    }

    /**
     * @return the suite of tests being tested
     */
    public static Test suite()
    {
        return new TestSuite( AppTest.class );
    }

    /**
     * Rigourous Test :-)
     * @throws MalformedURLException 
     */
    public void testApp() throws MalformedURLException
    {
    	
    	 WebDriver driver;	
    	 DesiredCapabilities dr=null;
    	 dr=DesiredCapabilities.chrome();
    	 driver=new RemoteWebDriver(new URL("http://52.90.151.23:4444/wd/hub"), dr);
    	 
    	 // driver.get("http://localhost:9090/demo/#/");
    	  driver.get("http://35.174.137.193:9090/demo/#/");
          // Print a Log In message to the screen
          System.out.println("Successfully opened the demo App");

    		//Wait for 5 Sec
    		try {
    			Thread.sleep(2000);
    		} catch (InterruptedException e) {
    			// TODO Auto-generated catch block
    			e.printStackTrace();
    		}
    		
    		driver.findElement(By.id("uname")).sendKeys("test1");
    		driver.findElement(By.id("age")).sendKeys("12");
    		driver.findElement(By.id("salary")).sendKeys("23000");
    		
    		driver.findElement(By.xpath("//button[text()='RESET']")).click();
    		
    		//assertTrue("Age feild should be blank", driver.findElement(home.txt_Age()).getText().isEmpty());
    	//	assertTrue("Salary feild should be blank", driver.findElement(home.txt_Salary()).getText().isEmpty());
    		
        assertTrue( true );
    }
}
